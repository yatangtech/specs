Pod::Spec.new do |s|
  s.name             = 'YTScanView'
  s.version          = '0.1.3'
  s.summary          = '雅堂iOS扫码组件'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://Dev_Min@bitbucket.org/yatangtech/ytscanview.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'YaTang_Dev_Hemin' => '286888980@qq.com' }
  s.source           = { :git => 'https://Dev_Min@bitbucket.org/yatangtech/ytscanview.git', :tag => s.version.to_s }
  s.requires_arc = true
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'YTScanView/Classes/**/*'
  # s.resource = 'YTScanView/Assets/Resources/CodeScan.bundle'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'Foundation', 'UIKit', 'AVFoundation', 'CoreGraphics', 'CoreMedia', 'CoreVideo', 'ImageIO', 'QuartzCore'

end

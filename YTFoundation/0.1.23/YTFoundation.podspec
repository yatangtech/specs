#
# Be sure to run `pod lib lint YTFoundation.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YTFoundation'
  s.version          = '0.1.23'
  s.summary          = '雅堂iOS基础库，包括Category，工具类等'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'http://bitbucket.org/lixiang_yatang/ytfoundation'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lixiang' => 'lixiang@yatang.cn' }
  s.source           = { :git => 'https://lixiang_yatang@bitbucket.org/yatangtech/ytfoundation.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'Classes/**/*'
  
  s.frameworks = 'UIKit'

  #s.public_header_files = 'Classes/YTFoundation.h'
end
